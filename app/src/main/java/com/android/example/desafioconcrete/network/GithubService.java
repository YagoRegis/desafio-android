package com.android.example.desafioconcrete.network;

import com.android.example.desafioconcrete.models.PullRequest;
import com.android.example.desafioconcrete.models.RepositoriesList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by -Yago- on 24/06/2017.
 */

public interface GithubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<RepositoriesList> getRepositoriesList(@Query("page") int page);

    @GET("repos/{login}/{name}/pulls")
    Call<ArrayList<PullRequest>> getPullRequestList(@Path("login") String login, @Path("name")String name);
}
