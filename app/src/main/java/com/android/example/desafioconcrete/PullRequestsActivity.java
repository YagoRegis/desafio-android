package com.android.example.desafioconcrete;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.example.desafioconcrete.adapter.PullRequestsAdapter;
import com.android.example.desafioconcrete.models.PullRequest;
import com.android.example.desafioconcrete.network.GithubApi;
import com.android.example.desafioconcrete.network.GithubService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequestsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private ArrayList<PullRequest> pullRequests;
    private PullRequestsAdapter adapter;
    private GithubService githubService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);
        initView();
        loadData();
    }

    private void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.rv_pull_requests);
        progressBar = (ProgressBar) findViewById(R.id.pb_pull_requests);
        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void loadData() {
        githubService = GithubApi.getRetrofit().create(GithubService.class);
        callPullRequestsListAPI().enqueue(new Callback<ArrayList<PullRequest>>() {
            @Override
            public void onResponse(Call<ArrayList<PullRequest>> call, Response<ArrayList<PullRequest>> response) {
                pullRequests = fetchData(response);
                progressBar.setVisibility(View.GONE);
                adapter = new PullRequestsAdapter(pullRequests, getApplicationContext());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ArrayList<PullRequest>> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private Call<ArrayList<PullRequest>> callPullRequestsListAPI() {
        Intent intent = getIntent();
        String login = intent.getStringExtra(Intent.EXTRA_TEXT);
        String name = intent.getStringExtra("NAME");
        return githubService.getPullRequestList(login, name);
    }

    private ArrayList<PullRequest> fetchData(Response<ArrayList<PullRequest>> response) {
        return response.body();
    }
}
