package com.android.example.desafioconcrete.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by -Yago- on 24/06/2017.
 */

public class GithubApi {
    private static Retrofit retrofit = null;

    private static final String BASE_URL = "https://api.github.com/";

    public static Retrofit getRetrofit() {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();

        return retrofit;
    }
}
