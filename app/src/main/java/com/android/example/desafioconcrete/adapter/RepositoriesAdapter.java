package com.android.example.desafioconcrete.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.example.desafioconcrete.PullRequestsActivity;
import com.android.example.desafioconcrete.R;
import com.android.example.desafioconcrete.models.Repositorie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by -Yago- on 24/06/2017.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder> {
    private ArrayList<Repositorie> repositories;
    private Context mContext;

    public RepositoriesAdapter(ArrayList<Repositorie> repositories, Context mContext) {
        this.repositories = repositories;
        this.mContext = mContext;
    }

    @Override
    public RepositoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repositorie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoriesAdapter.ViewHolder holder, int position) {
        Picasso.with(mContext).load(repositories.get(position).getOwner().getAvatarUrl()).fit().centerCrop()
                .into(holder.iv_repo_avatar);
        holder.tv_repo_username.setText(repositories.get(position).getOwner().getLogin());
        holder.tv_repo_name.setText(repositories.get(position).getFullName());
        holder.tv_repo_description.setText(repositories.get(position).getDescription());
        holder.tv_repo_forks.setText(repositories.get(position).getForksCount().toString());
        holder.tv_repo_stars.setText(repositories.get(position).getStargazersCount().toString());
    }


    @Override
    public int getItemCount() {
        return repositories.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_repo_username, tv_repo_name, tv_repo_description, tv_repo_forks, tv_repo_stars;
        private ImageView iv_repo_avatar;

        public ViewHolder(final View itemView) {
            super(itemView);

            iv_repo_avatar = (ImageView) itemView.findViewById(R.id.iv_repo_avatar);
            tv_repo_username = (TextView) itemView.findViewById(R.id.tv_repo_username);
            tv_repo_name = (TextView) itemView.findViewById(R.id.tv_repo_name);
            tv_repo_description = (TextView) itemView.findViewById(R.id.tv_repo_description);
            tv_repo_forks = (TextView) itemView.findViewById(R.id.tv_repo_forks);
            tv_repo_stars = (TextView) itemView.findViewById(R.id.tv_repo_stars);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Context context = itemView.getContext();

                    Intent intent = new Intent(context, PullRequestsActivity.class);
                    String login = repositories.get(position).getOwner().getLogin();
                    String name = repositories.get(position).getName();
                    intent.putExtra("NAME", name);
                    intent.putExtra(Intent.EXTRA_TEXT, login);

                    context.startActivity(intent);
                }
            });
        }
    }
}
