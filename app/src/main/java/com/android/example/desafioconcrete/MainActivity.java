package com.android.example.desafioconcrete;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.example.desafioconcrete.adapter.RepositoriesAdapter;
import com.android.example.desafioconcrete.models.Repositorie;
import com.android.example.desafioconcrete.models.RepositoriesList;
import com.android.example.desafioconcrete.network.GithubApi;
import com.android.example.desafioconcrete.network.GithubService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private LinearLayoutManager layoutManager;

    private ArrayList<Repositorie> repositories;
    private ArrayList<Repositorie> repositories2;
    private RepositoriesAdapter adapter;
    private GithubService githubService;

    private int CURRENT_PAGE = 1;
    private int PAGE_SIZE = 30;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        loadData();
    }

    private void initView() {
        recyclerView = (RecyclerView)findViewById(R.id.rv_repositories);
        progressBar = (ProgressBar)findViewById(R.id.pb_repositories);
        recyclerView.setHasFixedSize(false);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
    }

    private void loadData() {
        githubService = GithubApi.getRetrofit().create(GithubService.class);
        callRepositoriesListAPI().enqueue(new Callback<RepositoriesList>() {
            @Override
            public void onResponse(Call<RepositoriesList> call, Response<RepositoriesList> response) {
                repositories = fetchData(response);
                progressBar.setVisibility(View.GONE);
                adapter = new RepositoriesAdapter(repositories, getApplicationContext());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<RepositoriesList> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private Call<RepositoriesList> callRepositoriesListAPI() {
        return githubService.getRepositoriesList(CURRENT_PAGE);
    }

    private ArrayList<Repositorie> fetchData(Response<RepositoriesList> response) {
        RepositoriesList repositoriesList = response.body();
        return repositoriesList.getItems();
    }

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition() - 2;

            if (!isLoading && !isLastPage) {
                if ((visibleItemCount + lastVisibleItemPosition) >= totalItemCount
                        && lastVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
        }
    };

    private void loadMoreItems() {
        isLoading = true;

        CURRENT_PAGE++;

        callRepositoriesListAPI().enqueue(new Callback<RepositoriesList>() {
            @Override
            public void onResponse(Call<RepositoriesList> call, Response<RepositoriesList> response) {
                isLoading = false;

                if(!response.isSuccessful()) {
                    int responseCode = response.code();
                    switch (responseCode) {
                        case 504:
                            break;
                        case 400:
                            isLastPage = true;
                    }
                    return;
                }

                repositories2 = fetchData(response);
                for (Repositorie repositorie : repositories2) {
                    repositories.add(repositorie);
                    adapter.notifyItemInserted(repositories.size() - 1);
                }
                repositories2 = null;
            }

            @Override
            public void onFailure(Call<RepositoriesList> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }
}
