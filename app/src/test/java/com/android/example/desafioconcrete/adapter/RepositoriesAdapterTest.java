package com.android.example.desafioconcrete.adapter;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.example.desafioconcrete.BuildConfig;
import com.android.example.desafioconcrete.models.Owner;
import com.android.example.desafioconcrete.models.Repositorie;
import com.android.example.desafioconcrete.models.RepositoriesList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

/**
 * Created by -Yago- on 27/06/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 25)
public class RepositoriesAdapterTest {
    private RepositoriesAdapter adapter;
    private Repositorie repositorie1;
    private Repositorie repositorie2;
    private Context context;
    LinearLayout parent;
    private View view;

    private Owner owner;

    public RepositoriesAdapterTest() {
        super();
    }

    @Before
    public void setUp() throws Exception {
        Application application = RuntimeEnvironment.application;
        application.getPackageName();
        context = application;
        parent = new LinearLayout(context);
        view = new View(context);

        ArrayList<Repositorie> arrayList = new ArrayList<>();
        owner = new Owner("test", 1, "test", "test");
        repositorie1 = new Repositorie("test", "test", owner, "www.test.com.br", "test", 10, 10);
        repositorie2 = new Repositorie("test", "test", owner, "www.test.com.br", "test", 15, 15);
        arrayList.add(repositorie1);
        arrayList.add(repositorie2);

        adapter = new RepositoriesAdapter(arrayList, context);
    }

    @Test
    public void testOnCreateViewHolder() throws Exception {
        assertThat(adapter.onCreateViewHolder(parent, 0), instanceOf(RepositoriesAdapter.ViewHolder.class));
    }

    @Test
    public void testGetItemCount() throws Exception {
        assertEquals(2, adapter.getItemCount());
    }
}